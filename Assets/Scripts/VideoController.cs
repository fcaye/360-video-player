﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;

//Controlador del reproductor de video.
//Debe estar en el mismo objeto que el 
public class VideoController : MonoBehaviour {

	private VideoPlayer videoPlayer;
	public VideoClip firstClip;
	public VideoClip secondClip;

	public Button buttonVideo1;
	public Button buttonVideo2;
	public Button buttonPause;
	public Button buttonPlay;
	public Text initialText;

	private bool hasBegun;

	// Use this for initialization
	void Start () {

		videoPlayer = gameObject.GetComponent<VideoPlayer>();
		videoPlayer.playOnAwake = false;
		hasBegun = false;
	}
	public void FirstVideoButtonPressed(){

		Debug.Log("Video 1 Button Pressed");
		FirstTimePlaying();
		videoPlayer.clip = firstClip;
		videoPlayer.Play();
		buttonVideo1.interactable = false;
		buttonVideo2.interactable = true;
	}

	public void SecondVideoButtonPressed(){

		Debug.Log("Video 2 Button Pressed");
		FirstTimePlaying();
		videoPlayer.clip = secondClip;
		videoPlayer.Play();
		buttonVideo2.interactable = false;
		buttonVideo1.interactable = true;
	}

	public void PlayButtonPressed(){

		Debug.Log("Play Button Pressed");
		videoPlayer.Play();
	}

	public void PauseButtonPressed(){
		
		Debug.Log("Pause Button Pressed");
		videoPlayer.Pause();
	}

	private void FirstTimePlaying()
	{
		if(hasBegun == true)
		{
			return;
		}
		else{
			buttonPause.interactable = true;
			buttonPlay.interactable = true;
			initialText.enabled = false;
			hasBegun = true;
		}
		return;
	}
}
