﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
Script que utilizan los botones para activarse tras cierto tiempo
Este Script se añade al mismo objeto que tiene el script Button
Además, se añaden los Event Trigger Pointer Enter y Pointer Exit
En Pointer Enter se ejecuta el metodo Started Gazing
En Pointer Exit se ejecuta el metodo Stopped Gazing  
 */
public class TimedInput : MonoBehaviour {

	//Tiempo necesario para activar el boton

	public float timeToGaze = 1.5f;

	//Tiempo que se ha estado mirando el boton
	private float timeGazing = 0;

	//Referencia al boton en si
	private Button thisButton;

	private bool isGazing = false;

	void Start () {
		thisButton = gameObject.GetComponent<Button>();
	}
	
	void Update () {

		if(isGazing)
		{
			timeGazing += Time.deltaTime;

			if(timeGazing > timeToGaze)
			{
				thisButton.onClick.Invoke();
				timeGazing = 0;
			}
		}
	}


	public void StartedGazing(){

		isGazing = true;
	}

	public void StoppedGazing(){
		
		isGazing = false;
		timeGazing = 0;
	}
}
